﻿using System;
using System.Collections.Generic;

namespace MigratoryBirds
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> b1 = new List<int>{1, 4, 4, 4, 5, 3};// 4
            List<int> b2 = new List<int>{1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4};//3


            Console.WriteLine(migratoryBirds(b1));
            Console.WriteLine(migratoryBirds(b2));

        }

        static int migratoryBirds(List<int> arr) {

            var d = new Dictionary<int, int>();
            for (int i = 1; i < 6; i++)
            {
                d[i] = arr.FindAll(e => e == i).Count;
            }
            int count = 0;
            int birdMostSpotted = 0;
            foreach (var item in d.Keys)
            {
                if(d[item] > count){
                    count = d[item];
                    birdMostSpotted = item;
                }
            }

            return birdMostSpotted;

        }
    }
}
